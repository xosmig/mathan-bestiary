
.PHONY: default open run compile

source:=bestiary.hw
result:=bestiary.pdf
target:=bestiary_full.pdf
compile_silent:=\
	make compile > /dev/null 2>/dev/null; \
	res=$$?; \
	test "0$$res" = "00" && echo OK || echo FAIL; \
	date +%T; \
	test "0$$res" = "00"

default: compile

compile:
	hwformat $(source)
	cp $(result) $(target)

open: compile
	google-chrome $(target)

run:
	while (true); do \
		$(compile_silent); \
		sleep 1; \
	done

push:
	$(compile_silent) && \
	git add --all; \
	git commit -m "$$(date)"; \
	git push
